default['ubuntu_base']['locales'] = %w(en_GB.utf8 de_DE.utf8)
default['ubuntu_base']['timezone'] = 'Europe/Berlin'

if node['platform_version'] == '16.04'
	default['locales']['locale_file'] = '/etc/locale.gen'
	default['ubuntu_base']['php_version'] = '7.1' # either 7.0 or 7.1
else
	default['ubuntu_base']['php_version'] = '-' # option is not available for older platforms
end