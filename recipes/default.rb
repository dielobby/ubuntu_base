#
# Cookbook Name:: ubuntu_base
# Recipe:: default
#
# Copyright Paul Ilea
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

data_bag = data_bag_item('apps', 'global')

#########################
### Update the system ###
#########################

# apt-get crashes if the directory isn't created before it's execution
directory '/var/www/html' do
	recursive true
	action :create
end

execute 'apt-get -y update'

# no updated anymore as this can cause critical problems (better use a fresh updated box)
#execute 'apt-get -y upgrade'

###################
### Update ruby ###
###################

if node['platform_version'] === '16.04'
	package 'ruby'
else
	apt_repository('ruby-ng') do
		uri 'http://ppa.launchpad.net/brightbox/ruby-ng/ubuntu'
		deb_src true
		trusted true
		keyserver 'keyserver.ubuntu.com'
		key 'C3173AA6'
		distribution 'trusty'
		components [:main]
		arch 'amd64,i386'
		action :add
	end

	package 'ruby2.2', {:action => :upgrade}
end

###########################
### Set system timezone ###
###########################

timezone node['ubuntu_base']['timezone']

###################################
### Add some additional recipes ###
###################################

include_recipe 'locales'
include_recipe 'ssh_known_hosts'

########################################
### Install some additional packages ###
########################################

include_recipe 'apt'
include_recipe 'vim'
include_recipe 'build-essential'
include_recipe 'git'
include_recipe 'rsync'
package 'unzip'

#################################################
### Fix the user/group id of the vagrant user ###
#################################################

bash 'changeUserId' do
	cwd '/home/vagrant/'
	code <<-EOF
		# don't change the group id as this is usually a lower one on e.g. OSX that is already taken by the system
		#cat /etc/group | sed s'/vagrant:x:1000/vagrant:x:#{data_bag['groupId']}/' > /home/vagrant/group.bak
		#mv /home/vagrant/group.bak /etc/group

		sudo usermod -aG #{data_bag['groupId']} vagrant

		cat /etc/passwd | sed s'/vagrant:x:1000:1000/vagrant:x:#{data_bag['userId']}:1000/' > /home/vagrant/passwd.bak
		mv /home/vagrant/passwd.bak /etc/passwd

		chown -R vagrant:vagrant /home/vagrant
	EOF
	action :run
end

##########################################################
### Install cachefilesd to improve the NFS performance ###
##########################################################

package 'cachefilesd' do
	action :install
end

file '/etc/default/cachefilesd' do
	content <<-EOf
	    RUN=yes
	EOf
	action :create
	mode 0755
end

##########################################
### Provide common configuration files ###
##########################################

template '/home/vagrant/.bashrc' do
	source 'bashrc'
	owner 'vagrant'
	group 'vagrant'
end

template '/home/vagrant/.vimrc' do
	source 'vimrc'
	owner 'vagrant'
	group 'vagrant'
end

template '/home/vagrant/.vimrc.less' do
	source 'vimrc_less'
	owner 'vagrant'
	group 'vagrant'
end

template '/home/vagrant/.gitconfig' do
	source 'gitconfig.erb'
	owner 'vagrant'
	group 'vagrant'

	variables(
		{
			:author => data_bag['name'],
			:authorEmail => data_bag['email']
		}
	)
end

###################
### Add locales ###
###################

locales 'Add locales' do
	locales node['ubuntu_base']['locales']
end

################
### Add Logo ###
################

template '/etc/motd' do
	source 'motd'
	owner 'root'
	group 'root'
end
